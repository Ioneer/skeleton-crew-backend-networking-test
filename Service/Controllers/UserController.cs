﻿using Backend.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Service.Controllers
{
    public class UserController : ApiController
    {
        private readonly IUserManager userManager;

        public UserController(IUserManager userManager)
        {
            this.userManager = userManager;
        }

        // Get /api/users
        public IHttpActionResult GetUsers()
        {
            var users = this.userManager.GetUsers();
            return Ok(users);
        }

    }
}