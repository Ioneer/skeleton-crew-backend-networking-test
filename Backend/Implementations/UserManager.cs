﻿using Backend.Entities;
using Backend.Interfaces;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Implementations
{
    public class UserManager : IUserManager
    {
        public int AddUser(string name, double x = 0, double y = 0)
        {
            using (var context = new Context())
            {
                var result = context.Users.Add(new UserEntity { Name = name, X = x, Y = y });
                context.SaveChanges();

                return result.Id;
            }
        }

        public List<User> GetUsers()
        {
            using (var context = new Context())
            {
                var users = context.Users.AsEnumerable().Select(u => new User(u.Id, u.Name, u.X, u.Y)).ToList();
                return users;
            }
        }

        public void UpdateUserPosition(int id, double x, double y)
        {
            using (var context = new Context())
            {
                var user = context.Users.Find(id);

                if(user != null)
                {
                    user.X = x;
                    user.Y = y;
                    context.SaveChanges();
                }
            }
        }

        public void RemoveAllUsers()
        {
            using (var context = new Context())
            {
                context.Database.ExecuteSqlCommand("TRUNCATE TABLE [UserEntities]");
            }
        }
    }
}
