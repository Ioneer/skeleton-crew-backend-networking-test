﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Interfaces
{
    public interface IUserManager
    {
        List<User> GetUsers();

        int AddUser(string name, double x = 0.0, double y = 0.0);

        void UpdateUserPosition(int id, double x, double y);

        void RemoveAllUsers();

    }
}
