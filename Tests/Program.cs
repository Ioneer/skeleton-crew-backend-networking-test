﻿using Backend.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    class Program
    {
        static void Main(string[] args)
        {
            var manager = new UserManager();

            manager.RemoveAllUsers();

            manager.AddUser("Jack");
            manager.AddUser("Peter", 2, 5);
            manager.AddUser("Marry", 7, 3);

            var res = manager.GetUsers();

            foreach (var r in res)
            {
                Console.WriteLine($"{r.Username}, at ({r.PositionX}, {r.PositionY}).");
            }
            Console.WriteLine();
            manager.UpdateUserPosition(res[0].Id, 0, 99);


            res = manager.GetUsers();

            foreach (var r in res)
            {
                Console.WriteLine($"{r.Username}, at ({r.PositionX}, {r.PositionY}).");
            }

            //manager.RemoveAllUsers();
        }
    }
}
