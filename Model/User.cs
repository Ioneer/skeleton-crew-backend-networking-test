﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class User
    {
        public User(int id, string name, double x, double y)
        {
            this.Id = id;
            this.Username = name;
            this.PositionX = x;
            this.PositionY = y;
        }

        public int Id { get; set; }

        public string Username { get; set; }

        public double PositionX { get; set; }

        public double PositionY { get; set; }
    }
}
