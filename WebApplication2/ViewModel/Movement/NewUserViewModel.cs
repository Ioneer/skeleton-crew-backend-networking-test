﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication2.ViewModel.Movement
{
    public class NewUserViewModel
    {
        [Required]
        public string Username { get; set; }
    }
}