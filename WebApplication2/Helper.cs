﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2
{
    public static class Helper
    {
        public static double Total = 0;
        public static List<History> History = new List<History>();
    }



    public class History
    {
        public string Name { get; set; }

        public double Number { get; set; }

        public double TotalThen { get; set; }
    }
}