﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class Addition
    {
        [Required]
        public double Number { get; set; }

        [Required]
        public string Name { get; set; }
    }
}