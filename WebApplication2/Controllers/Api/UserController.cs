﻿using Backend.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication2.ViewModel.Movement;

namespace Service.Controllers
{
    public class UserController : ApiController
    {
        private readonly IUserManager userManager;

        public UserController(IUserManager userManager)
        {
            this.userManager = userManager;
        }

        // Get /api/users
        public IHttpActionResult GetUsers()
        {
            var users = this.userManager.GetUsers();
            return Ok(users);
        }

        // Post /api/users
        public IHttpActionResult Post(NewUserViewModel user)
        {
            var result = this.userManager.AddUser(user.Username);
            return Ok(result);
        }

        // Put /api/users
        public IHttpActionResult Put(UpdatePositionViewModel newPosition)
        {
            this.userManager.UpdateUserPosition(newPosition.Id, newPosition.X, newPosition.Y);
            return Ok();
        }

        // Delete /api/users
        public IHttpActionResult Delete()
        {
            this.userManager.RemoveAllUsers();
            return Ok();
        }

    }
}