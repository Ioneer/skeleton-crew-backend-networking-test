﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication2.ViewModel.Movement;

namespace WebApplication2.Controllers.Api
{
    public class PrimeController : ApiController
    {
        // Get /api/prime
        public IHttpActionResult Post(FindNextPrimeViewModel number)
        {
            var nextPrime = this.NextPrime(number.Number);
            return Ok(nextPrime);
        }

        public int NextPrime(double input)
        {
            if(input < 2)
            {
                return 2; // The first prime number is 2.
            }

            double temp = Math.Ceiling(input);

            int number = (int) temp;
            if(input != number)
            {
                number--;
            }

            while (true)
            {
                bool isPrime = true;
                //increment the number by 1 each time
                number = number + 1;

                int squaredNumber = (int)Math.Sqrt(number);

                //start at 2 and increment by 1 until it gets to the squared number
                for (int i = 2; i <= squaredNumber; i++)
                {
                    //how do I check all i's?
                    if (number % i == 0)
                    {
                        isPrime = false;
                        break;
                    }
                }
                if (isPrime)
                {
                    return number;
                }
            }
        }
    }
}
