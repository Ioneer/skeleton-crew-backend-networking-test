﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using WebApplication2.ViewModel.Movement;
using Newtonsoft.Json.Linq;

namespace WebApplication2.Controllers
{
    public class MovementController : Controller
    {
        private static string cookieKey = "Username";

        // GET: Positions
        public ActionResult Index()
        {
            HttpCookie myCookie = Request.Cookies[cookieKey];

            if(myCookie != null)
            {
                //ViewBag.UserId = myCookie.Value;
                return View(new UserIdViewModel { Id = int.Parse(myCookie.Value) });
            }
            else
            {
                return RedirectToAction("Create");
            }

        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(NewUserViewModel user)
        {
            using (var client = new HttpClient())
            {
                // In production this would be stored in a config file. So we could easily use different databases depending on inviroment.
                var uri = new Uri(Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/api/");
                client.BaseAddress = uri;

                var responseTask = client.PostAsJsonAsync<NewUserViewModel>("user", user);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var read = result.Content.ReadAsAsync<int>();
                    read.Wait();
                    var id = read.Result;
                    
                    HttpCookie myCookie = new HttpCookie(cookieKey);
                    myCookie.Value = id.ToString();

                    myCookie.Expires = DateTime.Now.AddYears(50);

                    Response.Cookies.Add(myCookie);

                    return RedirectToAction("Index");
                }
                else
                {
                    return View("Error");
                }
            }
        }
    }
}