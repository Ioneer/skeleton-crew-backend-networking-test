﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class AdditionController : Controller
    {
        private double current;

        public AdditionController()
        {
            current = 0;
        }

        //// GET: Addition
        //public ActionResult Index()
        //{
        //    ViewBag.Total = this.current;

        //    return View();
        //}

        public ActionResult Index([Bind(Include = "Number,Name")]Addition addition)
        {
            if(addition != null && !string.IsNullOrEmpty(addition.Name))
            {
                var total = Helper.Total + addition.Number;
                Helper.Total = total;
                Helper.History.Add(
                    new History
                    {
                        Name = addition.Name,
                        Number = addition.Number,
                        TotalThen = total
                    });
            }
            ViewBag.Total = Helper.Total;
            ViewBag.Entries = Helper.History;
            return View();
        }
    }

    
}